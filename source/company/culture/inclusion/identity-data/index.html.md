---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data as of 2020-03-31

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 122       | 10.02%          |
| Based in EMEA                               | 322       | 28.46%          |
| Based in LATAM                              | 18        | 1.48%           |
| Based in NORAM                              | 755       | 62.04%          |
| **Total Team Members**                      | **1,217** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 858       | 70.50%          |
| Women                                       | 359       | 29.50%          |
| Other Gender Identities                     | 0         | 0.00%           |
| **Total Team Members**                      | **1,217** | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 61        | 76.25%          |
| Women in Leadership                         | 19        | 23.75%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **80**    | **100%**        |

| **Gender in Development**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Development                          | 418       | 82.28%          |
| Women in Development                        | 90        | 17.72%          |
| Other Gender Identities                     | 0         | 0.00%           |
| **Total Team Members**                      | **508**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.28%           |
| Asian                                       | 46        | 6.48%           |
| Black or African American                   | 22        | 3.10%           |
| Hispanic or Latino                          | 38        | 5.35%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 25        | 3.52%           |
| White                                       | 426       | 60.00%          |
| Unreported                                  | 151       | 21.27%          |
| **Total Team Members**                      | **710**   | **100%**        |

| **Race/Ethnicity in Development (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 18        | 8.96%           |
| Black or African American                   | 3         | 1.49%           |
| Hispanic or Latino                          | 9         | 4.48%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 3.48%           |
| White                                       | 127       | 63.18%          |
| Unreported                                  | 37        | 18.41%          |
| **Total Team Members**                      | **201**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 14.52%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 3.23%           |
| White                                       | 37        | 59.68%          |
| Unreported                                  | 14        | 22.58%          |
| **Total Team Members**                      | **62**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.16%           |
| Asian                                       | 107       | 8.79%           |
| Black or African American                   | 30        | 2.47%           |
| Hispanic or Latino                          | 60        | 4.93%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 35        | 2.88%           |
| White                                       | 679       | 55.79%          |
| Unreported                                  | 304       | 24.98%          |
| **Total Team Members**                      | **1,217** | **100%**        |

| **Race/Ethnicity in Development (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 51        | 10.04%          |
| Black or African American                   | 7         | 1.38%           |
| Hispanic or Latino                          | 26        | 5.12%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 14        | 2.76%           |
| White                                       | 285       | 56.10%          |
| Unreported                                  | 125       | 24.61%          |
| **Total Team Members**                      | **508**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 10        | 12.50%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.25%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 2.50%           |
| White                                       | 46        | 57.50%          |
| Unreported                                  | 21        | 26.25%          |
| **Total Team Members**                      | **80**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 16        | 1.31%           |
| 25-29                                       | 227       | 18.65%          |
| 30-34                                       | 332       | 27.28%          |
| 35-39                                       | 264       | 21.69%          |
| 40-49                                       | 260       | 21.36%          |
| 50-59                                       | 103       | 8.46%           |
| 60+                                         | 13        | 1.07%           |
| Unreported                                  | 2         | 0.16%           |
| **Total Team Members**                      | **1,217** | **100%**        |


Source: GitLab's HRIS, BambooHR
